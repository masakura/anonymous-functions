﻿using System.Linq.Expressions;

namespace ExpressionTrees;

public sealed class ExpressionTreeTest
{
    [Test]
    public void ShowMethodName()
    {
        Expression<Action> expression = () => Hello();

        Console.WriteLine(((MethodCallExpression) expression.Body).Method.Name);
    }

    [Test]
    public void ShowOperator()
    {
        Expression<Func<int, int, int>> expression = (int x, int y) => x + y;

        var binaryExpression = (BinaryExpression) expression.Body;

        Console.WriteLine($"{binaryExpression.Left} [{binaryExpression.NodeType}] {binaryExpression.Right}");
    }

    private static void Hello()
    {
    }
}