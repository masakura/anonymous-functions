﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ExpressionTrees;

public sealed class EntityFrameworkTest
{
    private ApplicationDbContext _db;

    [SetUp]
    public void SetUp()
    {
        _db = new ApplicationDbContext();
        _db.Database.EnsureCreated();
    }

    [TearDown]
    public void TearDown()
    {
        _db.Dispose();
    }

    [Test]
    public void Test1()
    {
        _db.Employees.ToArray();
    }

    [Test]
    public void Test2()
    {
        _db.Employees.Select(e => new {First = e.FirstName}).ToArray();
    }

    [Test]
    public void Test3()
    {
        // ダメなのは SQLite3 provider だからだと思う...
        _db.Employees.Select(e => new
        {
            FullName = e.FirstName + " " + e.LastName,
            e.FirstName,
            e.LastName
        }).ToArray();
    }

    [Test]
    public void Test4()
    {
        // 書けない!
        // _db.Employees.Select(e => { return e.FirstName + " " + e.LastName; }).ToArray();
    }

    [Test]
    public void Test5()
    {
        _db.Employees.Where(e => e.FirstName == "山田").ToArray();
    }

    [Test]
    public void Test6()
    {
        _db.Employees.Where(e => e.FirstName.Length > 5).ToArray();
    }

    [Test]
    public void Test7()
    {
        var i = 5;
        _db.Employees.Where(e => e.FirstName.Length > i).ToArray();
    }

    [Test]
    public void Test8()
    {
        _db.Employees.Where(e => MyEquals(e.FirstName, "山田")).ToArray();
    }

    private static bool MyEquals(string x, string y)
    {
        return x == y;
    }

    [Test]
    public void Test9()
    {
        _db.Employees.Where(e => Equals(e.FirstName, "山田")).ToArray();
    }

    private static bool Equals(string x, string y)
    {
        throw new NotImplementedException();
    }
}

public sealed record Employee([property: Key] string FirstName, string LastName);

public sealed class ApplicationDbContext : DbContext
{
    public DbSet<Employee> Employees { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        base.OnConfiguring(options);

        options.UseSqlite("Data Source=sample.db");
        options.UseLoggerFactory(new ConsoleLoggingFactory());
    }
}

public sealed class ConsoleLoggingFactory : ILoggerFactory, ILogger
{
    public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception,
        Func<TState, Exception?, string> formatter)
    {
        if (eventId.Name?.Contains("CommandExecuted") ?? false)
        {
            Console.WriteLine("------------------------");
            Console.WriteLine(formatter(state, exception));
        }
    }

    public bool IsEnabled(LogLevel logLevel)
    {
        return true;
    }

    public IDisposable BeginScope<TState>(TState state)
    {
        throw new NotImplementedException();
    }

    public void Dispose()
    {
    }

    public ILogger CreateLogger(string categoryName)
    {
        return this;
    }

    public void AddProvider(ILoggerProvider provider)
    {
        throw new NotImplementedException();
    }
}