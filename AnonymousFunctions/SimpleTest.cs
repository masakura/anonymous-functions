﻿namespace AnonymousFunctions;

public sealed class SimpleTest
{
    [Test]
    public void AnonymousMethod()
    {
        var f1 = delegate(int x, int y) { return x + y; };
    }

    [Test]
    public void LambdaExpression()
    {
        var f2 = (int x, int y) => x + y;
    }

    [Test]
    public void LambdaExpressionLong()
    {
        var f2 = (int x, int y) => { return x + y; };
    }
}