﻿namespace AnonymousFunctions;

public sealed class AutoVariableCapturedTest
{
    [Test]
    public void AutoVariable()
    {
        var i = 10;
        var f1 = () => Console.WriteLine(i);
    }
}