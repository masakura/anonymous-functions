﻿namespace AnonymousFunctions;

public sealed class BadCapturedTest
{
    [Test]
    public void BadCaptured()
    {
        var actions = new List<Action>();

        for (var i = 0; i < 10; i++) actions.Add(() => Console.WriteLine(i));

        actions[0]();
    }
}